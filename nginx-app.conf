# nginx-app.conf

# the upstream component nginx needs to connect to
upstream django {
    server unix:/home/docker/code/app.sock; # for a file socket
    # server 127.0.0.1:8001; # for a web port socket (we'll use this first)
}

# configuration of the server
server {
        listen 80 default_server;
        listen [::]:80 default_server;
        server_name _;
        return 301 https://$host$request_uri;
}

server {
    # the port your site will be served on, default_server indicates that this server block
    # is the block to use if no blocks match the server_name
    listen      443;
    server_name	yourname.example.com_xyx;
    charset     utf-8;
    client_max_body_size 75M;   # adjust to taste

    ssl                     on;
    ssl_certificate         /home/docker/code/micapp/ssl_certs/example_xyx.certchain.crt;
    ssl_certificate_key     /home/docker/code/micapp/ssl_certs/example_xyx.key;
    ssl_trusted_certificate /home/docker/code/micapp/ssl_certs/example_xyx.trustchain.pem;
    ssl_protocols           TLSv1 TLSv1.1 TLSv1.2;
    ssl_prefer_server_ciphers       on;
    ssl_ciphers             ECDHE-RSA-AES128-GCM-SHA256:ECDHE-ECDSA-AES128-GCM-SHA256:ECDHE-RSA-AES256-GCM-SHA384:ECDHE-ECDSA-AES256-GCM-SHA384:DHE-RSA-AES128-GCM-SHA256:kEDH+AESGCM:ECDHE-RSA-AES128-SHA256:ECDHE-ECDSA-AES128-SHA256:ECDHE-RSA-AES128-SHA:ECDHE-ECDSA-AES128-SHA:ECDHE-RSA-AES256-SHA384:ECDHE-ECDSA-AES256-SHA384:ECDHE-RSA-AES256-SHA:ECDHE-ECDSA-AES256-SHA:DHE-RSA-AES128-SHA256:DHE-RSA-AES128-SHA:DHE-RSA-AES256-SHA256:DHE-RSA-AES256-SHA:!aNULL:!eNULL:!EXPORT:!DSS:!DES:!RC4:!3DES:!MD5:!PSK;
    ssl_stapling            on;
    ssl_stapling_verify     on;
    resolver                8.8.4.4 8.8.8.8 valid=300s;
    resolver_timeout        10s;
    ssl_session_cache       shared:SSL:32m;
    ssl_buffer_size         8k;
    ssl_session_timeout     180m;

    location /site_media/static {
        alias /home/docker/code/micapp/micapp/site_media/static; # your Django project's static files - amend as required
    }

    # Finally, send all non-media requests to the Django server.
    location / {
        uwsgi_pass  django;
        include     /home/docker/code/uwsgi_params; # the uwsgi_params file you installed
    }
}
