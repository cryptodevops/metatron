from __future__ import unicode_literals

from django.db import models
from django.utils.timezone import now

class TestResult(models.Model):
    timestamp = models.DateTimeField('date recorded', default=now, null=True, blank=True)
    start_time = models.DateTimeField(default=now, null=True, blank=True)
    end_time = models.DateTimeField(default=now, null=True, blank=True)
    event_id = models.CharField(max_length=50, null=True, blank=True)
    event_name = models.CharField(max_length=50, null=True, blank=True)
    parent_event = models.PositiveIntegerField(null=True, blank=True)

    def __unicode__(self):
        return '%s - %s' % (self.event_id, self.event_name)
