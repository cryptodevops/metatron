from django.contrib import admin
from testarest.models import TestResult

class TestResultAdmin(admin.ModelAdmin):
    list_display = ("id","start_time","end_time","event_id","event_name","parent_event")
    search_fields = ("id","timestamp","event_id","event_name")

admin.site.register(TestResult, TestResultAdmin)
