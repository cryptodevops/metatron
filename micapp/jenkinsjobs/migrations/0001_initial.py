# -*- coding: utf-8 -*-
# Generated by Django 1.10.3 on 2017-04-04 22:55
from __future__ import unicode_literals

import dirtyfields.dirtyfields
from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='JenkinsJob',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=80)),
                ('description', models.CharField(blank=True, max_length=2000, null=True)),
                ('instance', models.CharField(max_length=80)),
                ('instance_name', models.CharField(max_length=30)),
                ('buildstatus', models.CharField(blank=True, max_length=20, null=True, verbose_name=b'Build Status')),
                ('lastbuilddatetime', models.DateTimeField(blank=True, null=True)),
                ('svnloc', models.CharField(blank=True, max_length=800, null=True, verbose_name=b'SVN Location')),
                ('gitloc', models.CharField(blank=True, max_length=800, null=True, verbose_name=b'GIT Location')),
                ('xmlconfig', models.TextField()),
                ('monitored', models.BooleanField(default=True)),
            ],
            bases=(dirtyfields.dirtyfields.DirtyFieldsMixin, models.Model),
        ),
    ]
