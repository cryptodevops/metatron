from django.db import models
import reversion
from dirtyfields import DirtyFieldsMixin

class JenkinsJob(DirtyFieldsMixin, models.Model):
    name = models.CharField(max_length=80)
    description = models.CharField(max_length=2000, null=True, blank=True)
    instance = models.CharField(max_length=80)
    instance_name = models.CharField(max_length=30)
    buildstatus = models.CharField(max_length=20, null=True, blank=True, verbose_name='Build Status')
    lastbuilddatetime = models.DateTimeField(blank=True, null=True)
    svnloc = models.CharField(max_length=800, null=True, blank=True, verbose_name='SVN Location')
    gitloc = models.CharField(max_length=800, null=True, blank=True, verbose_name='GIT Location')
    xmlconfig = models.TextField()
    monitored = models.BooleanField(default=True)

    def __unicode__(self):
        return '%s' % (self.name)
