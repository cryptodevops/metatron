from django.core.management.base import BaseCommand, CommandError
import subprocess, sys, shlex
sys.path.append('/home/docker/code/micapp')
from pycipher import jenkins_password

class Command(BaseCommand):
    help = 'Updates all of the Jenkins Jobs'

    def handle(self, *args, **options):

        cmd_string = '/usr/bin/python /home/docker/code/micapp/jobsync.py http://your_jenkins_address_xyx/ Jenkins_User_xyx ' + jenkins_password_xyx
        args = shlex.split(cmd_string)
        p = subprocess.Popen(args, stdout = subprocess.PIPE, stderr= subprocess.STDOUT)

        # this is a lot of hackery just to get subprocess to print every char to stdout
        while True:
            out = p.stdout.read(1)
            if out == '' and p.poll() != None:
                break
            if out != '':
                sys.stdout.write(out)
                sys.stdout.flush()
