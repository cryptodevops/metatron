from django.contrib import admin
from django.contrib.admin import ModelAdmin
import reversion
from reversion_compare.admin import CompareVersionAdmin
from django.contrib.admin.utils import flatten_fieldsets
from jenkinsjobs.models import JenkinsJob


class JenkinsJobAdmin(CompareVersionAdmin):
    search_fields = ('name','description','svnloc','gitloc','xmlconfig')
    list_display = ('name','desctags','buildstatus','lastbuilddatetime','svnlink','gitlink','joblink')
    list_per_page = 200

    def joblink(self, obj):
        return '<a href="%sjob/%s">%s</a>' % (obj.instance, obj.name, obj.instance_name)
    joblink.allow_tags = True
    joblink.short_description = 'JLink'
    joblink.admin_order_field = 'instance_name'

    def desctags(self, obj):
        return '%s' % (obj.description)
    desctags.allow_tags = True
    desctags.admin_order_field = 'description'
    desctags.short_description = 'Description'

    def svnlink(self, obj):
        return '<a href="%s/">%s</a>' % (obj.svnloc, obj.svnloc)
    svnlink.allow_tags = True
    svnlink.short_description = 'SVN Link'
    svnlink.admin_order_field = 'svnloc'
    
    def gitlink(self, obj):
        if obj.gitloc is None:
            return 'None' 
        this_link = obj.gitloc.replace("git@github.com:","https://github.com/").replace("git@gitsome","https://git.then/").replace(".git","")
        return '<a href="%s/">%s</a>' % (this_link, obj.gitloc)
    gitlink.allow_tags = True
    gitlink.short_description = 'GIT Link'
    gitlink.admin_order_field = 'gitloc'

    def suit_row_attributes(self, obj, request):
        class_map = {
            'ABORTED': 'alert-warning',
            'FAILURE': 'alert-danger',
            'NO_BUILDS': 'alert-info',
            'SUCCESS': 'alert-success',
            'UNSTABLE': 'alert-info',
        }

        css_class = class_map.get(obj.buildstatus)
        if css_class:
            return {'class': css_class}
#            return {'class': css_class, 'data': obj.name}

admin.site.register(JenkinsJob, JenkinsJobAdmin)
