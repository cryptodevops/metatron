import os,sys
import xml.etree.ElementTree as ET
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "micapp.settings")
import django
django.setup()
import reversion, requests
from jenkinsjobs.models import JenkinsJob
from django.db import transaction
from django.contrib.auth.models import User
from jenkinsapi.jenkins import Jenkins
from jenkinsapi.utils.requester import Requester

instance = sys.argv[1] 
instance_name = sys.argv[2] 
my_password = sys.argv[3] 
requests.packages.urllib3.disable_warnings()
j = Jenkins(instance, requester=Requester('example_user_xyx', my_password, baseurl=instance, ssl_verify=False))

try:
    sys.argv[4]
    mykeys = [sys.argv[4]]
except IndexError:
    mykeys = j.keys()
i=0
newjob = False
print "Jenkins server " + str(instance) + " reports " + str(len(mykeys)) + " jobs being managed"

for jobname in mykeys:
    i += 1
    try:
        x = JenkinsJob.objects.get(name=jobname, instance=instance)
    except JenkinsJob.DoesNotExist:
        newjob = True
        x = JenkinsJob()
    if not x.monitored:
        print "Skipping job due to monitored flag being false"
        continue
    #sys.stdout.write(str(instance_name) + ' - ' + str(i) + ' processing job ' + jobname)
    sys.stdout.write('.')
    thisjob = j.get_job(jobname)
    try:
        config = thisjob.get_config().encode('utf-8')
    except:
        config = 'unknown'
    try:
        lastbuild = thisjob.get_last_build()
        mystatus = lastbuild.get_status()
        mydatetime = lastbuild.get_timestamp()
    except:
        mystatus = 'NO_BUILDS'
    #sys.stdout.write(' - last build status is ' + str(mystatus) + '\n')
    if mystatus is None:
        mystatus = 'BUILDING'
    x.name = jobname
    x.instance = instance
    x.instance_name = instance_name
    x.xmlconfig = config
    x.buildstatus = mystatus
    x.lastbuilddatetime = mydatetime
    try:
        tree = ET.ElementTree(ET.fromstring(config))
        x.description = tree.find('description').text
    except:
        x.description = 'no description provided' 
    try:
        x.svnloc = tree.find('scm/locations/hudson.scm.SubversionSCM_-ModuleLocation/remote').text
    except:
        x.svnloc = 'no svn location'
    try:
        x.gitloc = tree.find('scm/userRemoteConfigs/hudson.plugins.git.UserRemoteConfig/url').text
    except:
        x.gitloc = 'None'
    if x.is_dirty():
        with transaction.atomic(), reversion.create_revision():
            reversion.set_user(User.objects.get(username__exact='admin'))
            if newjob:
                reversion.set_comment('script adding initial revision')
            else:
                reversion.set_comment('script updating fields ' + str(x.get_dirty_fields().keys()))
            sys.stdout.write("\nupdating record - " + jobname + str(x.get_dirty_fields().keys()))
            x.save()
    sys.stdout.flush()

# Delete any jobs that are no longer on Jenkins - and save a secret copy!
for jenkins_job in JenkinsJob.objects.filter(instance=instance):
    if jenkins_job.name not in mykeys:
        with reversion.create_revision():
            reversion.set_user(User.objects.get(username__exact='admin'))
            reversion.set_comment('deleting job no longer found on Jenkins')
            JenkinsJob.objects.filter(id=jenkins_job.id).delete()
            print "Deleting JenkinsJob not found in list: " + jenkins_job.name 

print "\nUpdated configs on " + str(len(mykeys)) + " jobs"
