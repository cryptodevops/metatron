#!/usr/bin/env python
import os
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "micapp.settings")
import django, requests
django.setup()
from djaws.models import Instance
requests.packages.urllib3.disable_warnings()

def check_ssl(url):
    try:
        req = requests.get(url, verify=True)
        print url + ' has a valid SSL certificate!'
    except requests.exceptions.SSLError:
        print url + ' has INVALID SSL certificate!'

for single_host in Instance.objects.all():
#    print 'AWS Host ' + single_host.name + ' has public DNS ' + single_host.publicdnsname
  if single_host.state == 'running':
    if single_host.publicdnsname:
        my_url = 'https://' + single_host.publicdnsname
        try:
            this_response = requests.get(my_url, verify=True, timeout=3)
            print "Actually Hearing from this" + single_host.name
        except requests.exceptions.SSLError:
            print "SSL Error (invalid certs) from " + single_host.name
        except requests.exceptions.ConnectTimeout:
            print "Timeout from " + single_host.name
        except requests.exceptions.ConnectionError:
            print "Connection Error from " + single_host.name
  else:
      print "Instance not running: " + single_host.name
