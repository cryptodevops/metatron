from django.views.generic.base import TemplateView

from svnlit.models import Changeset 
from django.core.management import call_command
from StringIO import StringIO


class HomePageView(TemplateView):

    template_name = "homepage.html"

    def get_context_data(self, **kwargs):
        context = super(HomePageView, self).get_context_data(**kwargs)
        try:
            my_groups = self.request.user.groups.values_list('name', flat=True)
            my_title = self.request.user.title
            my_manager = self.request.user.mymanager
        except:
            my_groups = ['No groups found'] 
            my_title = 'No title recorded' 
            my_manager = 'No manager recorded' 
            
        context['latest_changesets'] = Changeset.objects.all()[:5]
        context['my_groups'] = my_groups
        context['my_title'] = my_title
        context['my_manager'] = my_manager
        return context

class AWSyncView(TemplateView):

    template_name = "awsync.html"

    def get_context_data(self, **kwargs):

        my_output = StringIO()
        call_command('sync_instances', stdout=my_output)
        call_command('sync_rds_instances', stdout=my_output)
        call_command('sgsync', stdout=my_output)
        call_command('svnsync', stdout=my_output)
        context = super(AWSyncView, self).get_context_data(**kwargs)
        context['this_data'] = my_output.getvalue()
        return context
