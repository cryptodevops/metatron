import os,ldap
from django_auth_ldap.config import LDAPSearch, ActiveDirectoryGroupType

PROJECT_ROOT = os.path.abspath(os.path.join(os.path.dirname(__file__), os.pardir))
PACKAGE_ROOT = os.path.abspath(os.path.dirname(__file__))
BASE_DIR = PACKAGE_ROOT
DEBUG = True

AUTH_LDAP_SERVER_URI = "ldap://ldap_server_name_xyx"
AUTH_LDAP_BIND_DN = "ldap_bind_dn_xyx"
AUTH_LDAP_BIND_PASSWORD = "example_password_xyx"
AUTH_LDAP_USER_SEARCH = LDAPSearch("dc=toplevel_ldap_xyx,dc=local",ldap.SCOPE_SUBTREE, "(sAMAccountName=%(user)s)")
AUTH_LDAP_GLOBAL_OPTIONS = { ldap.OPT_X_TLS_REQUIRE_CERT: False, ldap.OPT_REFERRALS: False, }

AUTH_LDAP_GROUP_SEARCH = LDAPSearch("ou=groups,ou=toplevel_ldap_group_xyx,dc=toplevel_ldap_xyx,dc=local", ldap.SCOPE_SUBTREE, "(objectClass=group)")
AUTH_LDAP_FIND_GROUP_PERMS = True
AUTH_LDAP_GROUP_TYPE = ActiveDirectoryGroupType()
#AUTH_LDAP_MIRROR_GROUPS = False
AUTH_LDAP_MIRROR_GROUPS_EXCEPT = [ "example_users", "these_users", "some_users" ]
AUTH_LDAP_CACHE_GROUPS = True
AUTH_LDAP_GROUP_CACHE_TIMEOUT = 300

# LDAP attribute map typically used in Microsoft AD
AUTH_LDAP_USER_ATTR_MAP = { "first_name": "givenName", "last_name": "sn", "mymanager": "manager",
                            "email": "mail", "telephone": "telephoneNumber", "title": "title" }

AUTH_PROFILE_MODULE = 'displaydata.UserProfile'

ALLOWED_HOSTS = [ "*", ]
DATABASE_ROUTERS = ['someRouterClass_xyx']
DATABASES = {
    "default": {
        "ENGINE": "django.db.backends.mysql",
        "HOST": "example-db.us-east-1.rds.amazonaws.com",
        "NAME": "example_name_xyx",
        "USER": "example_user",
        "PASSWORD": "example_password",
        "STORAGE_ENGINE": "INNODB",
    },
    "another_db": {
        "ENGINE": "django.db.backends.mysql",
        "NAME": "example_name_xyx",
        "USER": "example_user",
        "PASSWORD": "example_password",
        "HOST": "example-other-db.us-east-1.rds.amazonaws.com",
        "PORT": "3306",
        'OPTIONS': {
                    'charset': 'latin1',
                    'use_unicode': True, },
    }
}

TIME_ZONE = "America/Los_Angeles"
#INTERNAL_IPS 
# Language code for this installation. All choices can be found here:
# http://www.i18nguy.com/unicode/language-identifiers.html
LANGUAGE_CODE = "en-us"

SITE_ID = int(os.environ.get("SITE_ID", 1))

# If you set this to False, Django will make some optimizations so as not
# to load the internationalization machinery.
USE_I18N = True

# If you set this to False, Django will not format dates, numbers and
# calendars according to the current locale.
USE_L10N = True 

# If you set this to False, Django will not use timezone-aware datetimes.
USE_TZ = True

# Absolute filesystem path to the directory that will hold user-uploaded files.
# Example: "/home/media/media.lawrence.com/media/"
MEDIA_ROOT = os.path.join(PACKAGE_ROOT, "site_media", "media")

# URL that handles the media served from MEDIA_ROOT. Make sure to use a
# trailing slash.
# Examples: "http://media.lawrence.com/media/", "http://example.com/media/"
MEDIA_URL = "/site_media/media/"

# Absolute path to the directory static files should be collected to.
# Don"t put anything in this directory yourself; store your static files
# in apps" "static/" subdirectories and in STATICFILES_DIRS.
# Example: "/home/media/media.lawrence.com/static/"
STATIC_ROOT = os.path.join(PACKAGE_ROOT, "site_media", "static")

# URL prefix for static files.
# Example: "http://media.lawrence.com/static/"
STATIC_URL = "/site_media/static/"

# Additional locations of static files
STATICFILES_DIRS = [
    os.path.join(PROJECT_ROOT, "static", "dist"),
]

STATICFILES_STORAGE = "django.contrib.staticfiles.storage.ManifestStaticFilesStorage"

# List of finder classes that know how to find static files in
# various locations.
STATICFILES_FINDERS = [
    "django.contrib.staticfiles.finders.FileSystemFinder",
    "django.contrib.staticfiles.finders.AppDirectoriesFinder",
]

# Make this unique, and don't share it with anybody.
SECRET_KEY = "y833*some_secret_key_goes_here_av2fa$onk=se@#d"

TEMPLATES = [
    {
        "BACKEND": "django.template.backends.django.DjangoTemplates",
        "DIRS": [
            os.path.join(PACKAGE_ROOT, "templates"),
        ],
        "APP_DIRS": True,
        "OPTIONS": {
            "debug": DEBUG,
            "context_processors": [
                "django.contrib.auth.context_processors.auth",
                "django.template.context_processors.debug",
                "django.template.context_processors.i18n",
                "django.template.context_processors.media",
                "django.template.context_processors.static",
                "django.template.context_processors.tz",
                "django.template.context_processors.request",
                "django.contrib.messages.context_processors.messages",
                "account.context_processors.account",
                "pinax_theme_bootstrap.context_processors.theme",
            ],
        },
    },
]

MIDDLEWARE = [
    "django.contrib.sessions.middleware.SessionMiddleware",
    "django.middleware.common.CommonMiddleware",
    "django.middleware.csrf.CsrfViewMiddleware",
    "django.contrib.auth.middleware.AuthenticationMiddleware",
    "django.contrib.auth.middleware.SessionAuthenticationMiddleware",
    "django.contrib.messages.middleware.MessageMiddleware",
    "django.middleware.clickjacking.XFrameOptionsMiddleware",
    "debug_toolbar.middleware.DebugToolbarMiddleware",
]

ROOT_URLCONF = "micapp.urls"

# Python dotted path to the WSGI application used by Django's runserver.
WSGI_APPLICATION = "micapp.wsgi.application"

INSTALLED_APPS = [
    "suit",
    "django.contrib.admin",
    "django.contrib.auth",
    "django.contrib.contenttypes",
    "django.contrib.messages",
    "django.contrib.sessions",
    "django.contrib.sites",
    "django.contrib.staticfiles",

    # theme
    "bootstrapform",
    "pinax_theme_bootstrap",

    # external
    "account",
    "pinax.eventlog",
    "pinax.webanalytics",

    # project
    "debug_toolbar",
    "reversion",
    "reversion_compare",
    "dirtyfields",
    "micapp",
    "djaws",
    "testarest",
    "displaydata",
    "jenkinsjobs",
    "svnlit",
    "django_extensions",
    "rest_framework",
]

REST_FRAMEWORK = {
    'DEFAULT_PERMISSION_CLASSES': [
        'rest_framework.permissions.DjangoModelPermissions'
    ]
}
# A sample logging configuration. The only tangible logging
# performed by this configuration is to send an email to
# the site admins on every HTTP 500 error when DEBUG=False.
# See http://docs.djangoproject.com/en/dev/topics/logging for
# more details on how to customize your logging configuration.
LOGGING = {
    "version": 1,
    "disable_existing_loggers": False,
    "filters": {
        "require_debug_false": {
            "()": "django.utils.log.RequireDebugFalse"
        }
    },
    "handlers": {
        "mail_admins": {
            "level": "ERROR",
            "filters": ["require_debug_false"],
            "class": "django.utils.log.AdminEmailHandler"
        }
    },
    "loggers": {
        "django.request": {
            "handlers": ["mail_admins"],
            "level": "ERROR",
            "propagate": True,
        },
    }
}

FIXTURE_DIRS = [
    os.path.join(PROJECT_ROOT, "fixtures"),
]

EMAIL_BACKEND = "django.core.mail.backends.console.EmailBackend"

ACCOUNT_OPEN_SIGNUP = False 
ACCOUNT_EMAIL_UNIQUE = True
ACCOUNT_EMAIL_CONFIRMATION_REQUIRED = False
ACCOUNT_LOGIN_REDIRECT_URL = "home"
ACCOUNT_LOGOUT_REDIRECT_URL = "home"
ACCOUNT_EMAIL_CONFIRMATION_EXPIRE_DAYS = 2
ACCOUNT_USE_AUTH_AUTHENTICATE = True

SVNLIT_CLIENT_TIMEOUT = 300

AUTHENTICATION_BACKENDS = [
    "django_auth_ldap.backend.LDAPBackend",
    "account.auth_backends.UsernameAuthenticationBackend",
]
