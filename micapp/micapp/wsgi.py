import os
import sys 
sys.path.append('/home/docker/code/micapp')
sys.path.append('/home/docker/code/micapp/micapp')
from django.core.wsgi import get_wsgi_application
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "settings")

application = get_wsgi_application()

