from django.conf import settings
from django.conf.urls import include, url
from django.conf.urls.static import static
from micapp.views import HomePageView, AWSyncView
from django.contrib import admin
from rest_framework import serializers, viewsets, routers
from testarest.models import TestResult 
from displaydata.views import userinfo, userview

# Serializers define the API representation.
class TestResultSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = TestResult 
        fields = ("id","start_time","end_time","event_id","event_name","parent_event") 

# ViewSets define the view behavior.
class TestResultViewSet(viewsets.ModelViewSet):
    queryset = TestResult.objects.all()
    serializer_class = TestResultSerializer

# Routers provide a way of automatically determining the URL conf.
router = routers.DefaultRouter()
router.register(r'testresults', TestResultViewSet)

urlpatterns = [
    url(r"^$", HomePageView.as_view(), name="home"),
    url(r"^syncview/", AWSyncView.as_view(), name="awsync"),
    url(r"^userinfo/", userinfo),
    url(r"^userview/", userview),
    url(r"^admin/", include(admin.site.urls)),
    url(r"^account/", include("account.urls")),
    url(r'^', include(router.urls)),
    url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework')),
]

if settings.DEBUG:
    import debug_toolbar
    urlpatterns = [
        url(r'^__debug__/', include(debug_toolbar.urls)),
    ] + urlpatterns

urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
