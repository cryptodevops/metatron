#!/usr/bin/env python
import os
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "micapp.settings")
import django
django.setup()
from django_auth_ldap.backend import LDAPBackend
ldap_backend = LDAPBackend()
ldap_backend.populate_user('example_user')
