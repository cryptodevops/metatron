from django.contrib import admin
import reversion
from reversion_compare.admin import CompareVersionAdmin
from django.contrib.admin.utils import flatten_fieldsets
from django.utils.http import urlencode
from djaws.models import Instance, RDSInstance, SecurityGroup, PublicKey, PublicKeyInstance, NodelogInstance, NodeLogSize, ProductTag, ProductTagInstance
from django.db.models import Count
import re

class InstanceAdmin(CompareVersionAdmin):
    search_fields = ('id','name','availabilityzone','securitygroups','instancetype','hypervisor','privatednsname','privateipaddress','imageid','publicdnsname','publicipaddress','vpcid','tags','state')
    list_display = ('id','name','availabilityzone','ami_link','instancetype','privateipaddress','publicipaddress','keyname','state','launchtime','vpc_link','ssh_link','amz_link')
    list_per_page = 200

    def ssh_link(self, obj):
        # look for EC2 tag which specifies a "login user", else use the Debian standard admin
        my_search = re.search('login_user\ \ \-\-\>\ \ (.*)$', str(obj.tags))
        if my_search:
            my_user = my_search.group(1) + '@'
        else:
            my_user = 'admin@'
        # note that a special URL handler will need to be setup on the client side to parse the URL correctly
        this = '<a href="ssh://' + my_user + obj.privateipaddress + ('' if obj.keyname == '' else ';i=~/.ssh/' + obj.keyname ) + \
               '">priv</a>-<a href="ssh://' + my_user + obj.publicipaddress + ('' if obj.keyname == '' else ';i=~/.ssh/' + obj.keyname ) + '">pub</a>'
        return this
    ssh_link.allow_tags = True

    def amz_link(self, obj):
        this = '<a href="https://console.aws.amazon.com/ec2/home?region=' + obj.region_name + '#Instances:search=' + obj.id + '">amz link</a> '
        return this
    amz_link.allow_tags = True

    def vpc_link(self, obj):
        this = '<a href="https://console.aws.amazon.com/vpc/home?region=' + obj.region_name + '#vpcs:filter=' + obj.vpcid + '">' + obj.vpcid + '</a> '
        return this
    vpc_link.allow_tags = True

    def ami_link(self, obj):
        this = '<a href="https://console.aws.amazon.com/ec2/home?region=' + obj.region_name + '#Images:visibility=owned-by-me;search=' + obj.imageid + \
               ';sort=desc:creationDate">' + obj.imageid + '</a>'
        return this
    ami_link.allow_tags = True

class RDSInstanceAdmin(CompareVersionAdmin):
    search_fields = ('resource_id','identifier','availability_zone','engine','engine_version','replication_role','endpoint_address','tags')
    list_display = ('identifier','rds_class','status','allocated_storage','availability_zone','multi_az','engine','engine_version','replication_role','endpoint_address','amz_link')
    list_per_page = 200

    def amz_link(self, obj):
        this = '<a href="https://console.aws.amazon.com/rds/home?region=' + obj.region_name + '#database:id=' + obj.identifier + ';is-cluster=false">amz link</a> '
        return this
    amz_link.allow_tags = True

class SecurityGroupAdmin(CompareVersionAdmin):
    search_fields = ('groupid','groupname','description','ownerid','vpcid')
    list_display = ('groupid','groupname','description','ownerid','vpcid')
    list_per_page = 200

class PublicKeyAdmin(admin.ModelAdmin):
    search_fields = ('publickey','fingerprint','bitlength','name','keytype')
    list_display = ('fingerprint','bitlength','name','keytype','keysearch','instance_count')
    list_per_page = 200

    def get_queryset(self, request):
        return PublicKey.objects.annotate(instance_count=Count('publickeyinstance__instance'))

    def instance_count(self, inst):
        return inst.instance_count
    instance_count.admin_order_field = 'instance_count'

    def key_truncated(self, obj):
        info = (obj.publickey[:40] + '..') if len(obj.publickey) > 40 else obj.publickey
        return info
    key_truncated.admin_order_field = 'publickey'

    def keysearch(self, obj):
        this = urlencode({ 'q' : obj.fingerprint}, doseq=True)
        return '<a href="/admin/djaws/publickeyinstance/?%s">link</a>' % this
    keysearch.allow_tags = True

class PublicKeyInstanceAdmin(admin.ModelAdmin):
    search_fields = ('instance__name','publickey__fingerprint')
    list_display = ('instance','publickey','ssh_link')
    list_per_page = 200

    def ssh_link(self, obj):
        this = '<a href="ssh://this.com%20-A%20-t%20%22sudo%20ssh%20' + str(obj.instance) + '">ssh link</a>' 
        return this
    ssh_link.allow_tags = True

class ProductTagAdmin(admin.ModelAdmin):
    search_fields = ('name',)
    list_display = ('name','instance_count','tagsearch')
    list_per_page = 200

    def get_queryset(self, request):
        return ProductTag.objects.annotate(instance_count=Count('producttaginstance__instance'))

    def instance_count(self, inst):
        return inst.instance_count
    instance_count.admin_order_field = 'instance_count'

    def tagsearch(self, obj):
        this = urlencode({ 'q' : obj.name}, doseq=True)
        return '<a href="/admin/djaws/producttaginstance/?%s">link</a>' % this
    tagsearch.allow_tags = True

class ProductTagInstanceAdmin(admin.ModelAdmin):
    search_fields = ('instance__name','producttag__name')
    list_display = ('instance','producttag')
    list_per_page = 200

class NodelogInstanceAdmin(admin.ModelAdmin):
    search_fields = ('instance__name','logsize')
    list_display = ('instance','nodelogsize','instancelink')
    list_per_page = 200

    def nodelogsize(self, obj):
       from django.template import defaultfilters
       my_size = defaultfilters.filesizeformat(obj.logsize * 1024)
       return my_size 
    nodelogsize.admin_order_field = 'logsize'

    def instancelink(self, obj):
        this = urlencode({ 'q' : obj.instance}, doseq=True)
        return '<a href="/admin/djaws/instance/?%s">instance link</a>' % this
    instancelink.allow_tags = True

class NodeLogSizeAdmin(admin.ModelAdmin):
    search_fields = ('instance__name','logsize','access_method')
    list_display = ('instance','hr_size','access_method','instancelink')
    list_per_page = 200

    def hr_size(self, obj):
       from django.template import defaultfilters
       my_size = defaultfilters.filesizeformat(obj.logsize * 1024)
       return my_size 
    hr_size.admin_order_field = 'logsize'

    def instancelink(self, obj):
        this = urlencode({ 'q' : obj.instance}, doseq=True)
        return '<a href="/admin/djaws/instance/?%s">instance link</a>' % this
    instancelink.allow_tags = True

#admin.site.register(PublicKey, PublicKeyAdmin)
#admin.site.register(PublicKeyInstance, PublicKeyInstanceAdmin)
#admin.site.register(ProductTag, ProductTagAdmin)
#admin.site.register(ProductTagInstance, ProductTagInstanceAdmin)
#admin.site.register(NodelogInstance, NodelogInstanceAdmin)
#admin.site.register(NodeLogSize, NodeLogSizeAdmin)
admin.site.register(Instance, InstanceAdmin)
admin.site.register(RDSInstance, RDSInstanceAdmin)
admin.site.register(SecurityGroup, SecurityGroupAdmin)
