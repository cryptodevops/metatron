from django.db import models
import reversion
from dirtyfields import DirtyFieldsMixin

class SecurityGroup(DirtyFieldsMixin, models.Model):
    groupid = models.CharField(max_length=20,primary_key=True)
    groupname = models.CharField(max_length=255)
    description = models.CharField(max_length=255, blank=True)
    ownerid = models.CharField(max_length=20, blank=True)
    vpcid = models.CharField(max_length=20, blank=True)

    def __unicode__(self):
        return '%s' % (self.groupname)

class Instance(DirtyFieldsMixin, models.Model):
    id = models.CharField(max_length=20,primary_key=True)
    name = models.CharField(max_length=90)
    hypervisor = models.CharField(max_length=20, blank=True)
    region_name = models.CharField(max_length=30, blank=True)
    imageid = models.CharField(max_length=30, blank=True)
    instancetype = models.CharField(max_length=30, blank=True)
    kernelid = models.CharField(max_length=30, blank=True)
    keyname = models.CharField(max_length=30, blank=True)
    state = models.CharField(max_length=30, blank=True)
    launchtime = models.DateTimeField(blank=True, null=True)
    availabilityzone = models.CharField(max_length=30, blank=True)
    privatednsname = models.CharField(max_length=60, blank=True)
    privateipaddress = models.CharField(max_length=15, blank=True)
    publicdnsname = models.CharField(max_length=60, blank=True)
    publicipaddress = models.CharField(max_length=15, blank=True)
    vpcid = models.CharField(max_length=12, blank=True)
    securitygroups = models.TextField(blank=True)
    tags = models.TextField(blank=True)

    def __unicode__(self):
        return '%s' % (self.name)

class RDSInstance(DirtyFieldsMixin, models.Model):
    resource_id = models.CharField(max_length=29,primary_key=True)
    identifier = models.CharField(max_length=90)
    region_name = models.CharField(max_length=30, blank=True)
    engine = models.CharField(max_length=20, blank=True)
    engine_version = models.CharField(max_length=20, blank=True)
    rds_class = models.CharField(max_length=20, blank=True)
    status = models.CharField(max_length=20, blank=True)
    allocated_storage = models.IntegerField()
    multi_az = models.CharField(max_length=10, blank=True)
    availability_zone = models.CharField(max_length=20, blank=True)
    secondary_az = models.CharField(max_length=20, blank=True)
    db_name = models.CharField(max_length=40, blank=True)
    db_securitygroups = models.TextField(blank=True)
    endpoint_address = models.CharField(max_length=100, blank=True)
    endpoint_port = models.SmallIntegerField(blank=True, null=True)
    create_time = models.DateTimeField(blank=True, null=True)
    replication_role = models.CharField(max_length=10, blank=True)
    replica_source = models.CharField(max_length=40, blank=True)
    replica_ids = models.TextField(blank=True)
    billing_code = models.CharField(max_length=40, blank=True)
    tags = models.TextField(blank=True)
 
    def __unicode__(self):
        return '%s' % (self.identifier)

class PublicKey(DirtyFieldsMixin, models.Model):
    publickey = models.CharField(max_length=1000)
    fingerprint = models.CharField(max_length=48, unique=True)
    bitlength = models.SmallIntegerField()
    name = models.CharField(max_length=200)
    keytype = models.CharField(max_length=10)

    def __unicode__(self):
        return '%s' % (self.fingerprint)

class ProductTag(DirtyFieldsMixin, models.Model):
    name = models.CharField(max_length=100, unique=True)

    def __unicode__(self):
        return '%s' % (self.name)

class ProductTagInstance(DirtyFieldsMixin, models.Model):
    instance = models.ForeignKey(Instance)
    producttag = models.ForeignKey(ProductTag, to_field='name')

    def __unicode__(self):
        return '%s - %s' % (self.instance, self.producttag)

class PublicKeyInstance(DirtyFieldsMixin, models.Model):
    instance = models.ForeignKey(Instance)
    publickey = models.ForeignKey(PublicKey, to_field='fingerprint')

    def __unicode__(self):
        return '%s - %s' % (self.instance, self.publickey)

class NodelogInstance(DirtyFieldsMixin, models.Model):
    instance = models.ForeignKey(Instance)
    logsize = models.IntegerField()

    def __unicode__(self):
        return '%s - %s' % (self.instance, self.logsize)

class NodeLogSize(DirtyFieldsMixin, models.Model):
    instance = models.ForeignKey(Instance)
    logsize = models.IntegerField()
    access_method = models.CharField(max_length=12)
    

    def __unicode__(self):
        return '%s - %s' % (self.instance, self.logsize)


reversion.register(SecurityGroup)
reversion.register(Instance)
reversion.register(RDSInstance)
