from django.core.management.base import BaseCommand, CommandError
from djaws.models import SecurityGroup
from django.db import transaction
from django.contrib.auth.models import User
import boto3, reversion, sys
sys.path.append('/home/docker/code/micapp')
from pycipher import access_key_dev, secret_access_key_dev, access_key_stddev, secret_access_key_stddev

class Command(BaseCommand):
    help = 'Update the DjAWS database with the current list of Security Groups'

    def handle(self, *args, **options):
      my_regions = ('us-east-1','us-west-2')
      for this_profile in ['default','dev']:
        if this_profile == 'dev':
            boto3.setup_default_session(aws_access_key_id=access_key_dev, aws_secret_access_key=secret_access_key_dev)
        else:
            boto3.setup_default_session(aws_access_key_id=access_key_stddev, aws_secret_access_key=secret_access_key_stddev)
        for single_region in my_regions:
            self.stdout.write('this profile is ' + this_profile)
            if (this_profile == 'dev' and single_region == 'us-east-1') or (this_profile == 'default' and single_region == 'us-west-2'):
                self.stdout.write("Skipping unused account profile " + this_profile + " and region combo " + single_region)
                continue
            ec2_client = boto3.client('ec2', region_name=single_region)
            self.stdout.write("Requesting security groups from region: " + single_region)

            newsg = False
            i=0
            for this_group in ec2_client.describe_security_groups()['SecurityGroups']:
                i += 1
                my_id = this_group['GroupId']
                try:
                    x = SecurityGroup.objects.get(groupid=my_id)
                except SecurityGroup.DoesNotExist:
                    newsg = True
                    x = SecurityGroup()
                x.groupid = my_id
                x.groupname = this_group['GroupName']
                x.description = this_group['Description']
                x.ownerid = this_group['OwnerId']
                if 'VpcId' in this_group:
                    x.vpcid = this_group['VpcId']
                if x.is_dirty():
                    with transaction.atomic(), reversion.create_revision():
                        reversion.set_user(User.objects.get(username__exact='admin'))
                        if newsg:
                            reversion.set_comment('script adding initial revision')
                            self.stdout.write("adding new record - " + x.groupname + str(x.get_dirty_fields().keys()))
                        else:
                            reversion.set_comment('script updating fields ' + str(x.get_dirty_fields().keys()))
                            self.stdout.write("updating record - " + x.groupname + str(x.get_dirty_fields().keys()))
                        x.save()

# Example format of Security Group returned from AMZ
#{u'Description': 'Example Group',
# u'GroupId': 'sg-a12345a6',
# u'GroupName': 'vpc',
# u'IpPermissions': [],
# u'IpPermissionsEgress': [{u'IpProtocol': '-1',
#   u'IpRanges': [{u'CidrIp': '0.0.0.0/0'}],
#   u'PrefixListIds': [],
#   u'UserIdGroupPairs': []}],
# u'OwnerId': '12345678',
# u'VpcId': 'vpc-'1a2b3c4d'}
