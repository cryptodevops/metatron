from django.core.management.base import BaseCommand, CommandError
from djaws.models import RDSInstance
from django.db import transaction
from django.contrib.auth.models import User
import reversion, boto3

class Command(BaseCommand):
    help = 'Update the DjAWS database with the current list of EBS volumes'

    def handle(self, *args, **options):
        my_regions = ('us-east-1','us-west-1','us-west-2')

        for single_region in my_regions:

            ec2_client = boto3.client('ec2', region_name=single_region)
            i=0
            self.stdout.write('Acquiring EBS volumes in region ' + single_region + '...')
            inst_list = []
            paginator = ec2_client.get_paginator('describe_volumes')
            p=0
            total_size = 0
            for page in paginator.paginate():
                p += 1
                #print "page is " + str(page)
                for obj in page['Volumes']:
                    i += 1; my_id = obj['VolumeId']; my_bc = ''
                    my_size = obj['Size']
                    total_size += my_size
                    self.stdout.write(str(i) + "total size is " + str(total_size))
