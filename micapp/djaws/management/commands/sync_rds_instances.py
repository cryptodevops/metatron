from django.core.management.base import BaseCommand, CommandError
from djaws.models import RDSInstance
from django.db import transaction
from django.contrib.auth.models import User
import reversion, boto3, sys
sys.path.append('/home/docker/code/micapp')
from pycipher import access_key_stddev, secret_access_key_stddev


class Command(BaseCommand):
    help = 'Update the DjAWS database with the current list of RDS instances'

    def handle(self, *args, **options):

        my_regions = ('us-east-1','us-west-1','us-west-2')
        for single_region in my_regions:

            rds_client = boto3.client('rds', region_name=single_region, aws_access_key_id=access_key_stddev, aws_secret_access_key=secret_access_key_stddev)
            i=0
            self.stdout.write('Acquiring RDS instances in region ' + single_region + '...')
            inst_list = []
            paginator = rds_client.get_paginator('describe_db_instances')
            p=0
            for page in paginator.paginate():
                p += 1
                for obj in page['DBInstances']:
                    i += 1; my_id = obj['DbiResourceId']; my_bc = ''
                    newinstance = False; my_sgs = []; my_tags = []
                    inst_list.append(my_id)
                    try:
                        x = RDSInstance.objects.get(resource_id=my_id)
                    except RDSInstance.DoesNotExist:
                        newinstance = True
                        x = RDSInstance()
                    my_arn = 'arn:aws:rds:' + obj['AvailabilityZone'][:-1] + ':AMZ_account_id_xyx:db:' + obj['DBInstanceIdentifier'] 
                    #print "working on " + obj['DBInstanceIdentifier'] + " ARN " + my_arn
                    #pprint(obj)
                    response2 = rds_client.list_tags_for_resource(ResourceName=my_arn)
                    if response2['TagList']:
                        for my_tag in response2['TagList']:
                            my_tags.append(my_tag['Key'] + "  -->  " + my_tag['Value'])
                            if my_tag['Key'] == 'Billing_Code': x.billing_code = my_tag['Value']


                    #print '{:40s}'.format(obj['DBInstanceIdentifier']) + " has billing code: " + '{:10s}'.format(my_bc) + " and zone: " + obj['AvailabilityZone'] + " - " + str(i) + " - " + str(p)
                    x.region_name = single_region 
                    if 'DbiResourceId' in obj: x.resource_id = my_id
                    if 'DBInstanceIdentifier' in obj: x.identifier = obj['DBInstanceIdentifier']
                    if 'Engine' in obj: x.engine = obj['Engine']
                    if 'EngineVersion' in obj: x.engine_version = obj['EngineVersion']
                    if 'DBInstanceClass' in obj: x.rds_class = obj['DBInstanceClass']
                    if 'DBInstanceStatus' in obj: x.status = obj['DBInstanceStatus']
                    if 'AllocatedStorage' in obj: x.allocated_storage = obj['AllocatedStorage']
                    if 'MultiAZ' in obj: x.multi_az = 'True' 
                    else:  x.multi_az = 'False' 
                    if 'AvailabilityZone' in obj: x.availability_zone = obj['AvailabilityZone']
                    if 'SecondaryAvailabilityZone' in obj: x.secondary_az = obj['SecondaryAvailabilityZone']
                    if 'DBName' in obj: x.db_name = obj['DBName']
                    if 'DBSecurityGroups' in obj:
                        for my_sg in obj['DBSecurityGroups']:
                            my_sgs.append(my_sg['DBSecurityGroupName'])
                    my_sgs.sort(); x.db_securitygroups = "\n".join(my_sgs)
                    if 'Endpoint' in obj: x.endpoint_address = obj['Endpoint']['Address']
                    if 'Endpoint' in obj: x.endpoint_port = obj['Endpoint']['Port']
                    if 'InstanceCreateTime' in obj: x.create_time = obj['InstanceCreateTime'].replace(microsecond=0)
                    if 'ReadReplicaDBInstanceIdentifiers' in obj:
                        x.replication_role = 'master'; x.replica_ids = "\n".join(obj['ReadReplicaDBInstanceIdentifiers']) 
                    if 'ReadReplicaSourceDBInstanceIdentifier' in obj:
                        x.replication_role = 'replica'; x.replica_source = obj['ReadReplicaSourceDBInstanceIdentifier']
                    my_tags.sort(); x.tags = "\n".join(my_tags)
                    with transaction.atomic(), reversion.create_revision():
                        if x.is_dirty():
                            reversion.set_user(User.objects.get(username__exact='admin'))
                            if newinstance:
                                reversion.set_comment('script adding initial revision')
                                self.stdout.write("adding record - " + x.identifier + " - " + str(x.get_dirty_fields().keys()))
                            else:
                                reversion.set_comment('script updating fields ' + str(x.get_dirty_fields().keys()))
                                self.stdout.write("updating record - " + x.identifier + " - " + str(x.get_dirty_fields().keys()))
                            x.save()
            # check for potentially deleted instances (pd_instance)
            self.stdout.write('    - found a total of ' + str(len(inst_list)) + ' RDS instance(s)')
            for pd_instance in RDSInstance.objects.filter(region_name=single_region).values_list('resource_id', flat=True):
                if pd_instance not in inst_list:
                    with reversion.create_revision():
                        reversion.set_user(User.objects.get(username__exact='admin'))
                        reversion.set_comment('deleting RDS instance no longer found on AWS')
                        RDSInstance.objects.filter(resource_id=pd_instance).delete()
                        self.stdout.write("Deleting RDS instance not found in list: " + pd_instance)
