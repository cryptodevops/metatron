from django.core.management.base import BaseCommand, CommandError
from djaws.models import Instance,SecurityGroup,ProductTag,ProductTagInstance
from django.db import transaction
from django.contrib.auth.models import User
import reversion, boto3, sys
sys.path.append('/home/docker/code/micapp')
from pycipher import access_key_dev, secret_access_key_dev, access_key_stddev, secret_access_key_stddev

class Command(BaseCommand):
    help = 'Update the DjAWS database with the current list of instances'

    def handle(self, *args, **options):
      my_regions = ('us-east-1','us-west-2')
      for this_profile in ['default','dev']:
        if this_profile == 'dev':
            boto3.setup_default_session(aws_access_key_id=access_key_dev, aws_secret_access_key=secret_access_key_dev)
        else:
            boto3.setup_default_session(aws_access_key_id=access_key_stddev, aws_secret_access_key=secret_access_key_stddev)
        for single_region in my_regions:
            self.stdout.write('this profile is ' + this_profile + 'xxx')
            if (this_profile == 'dev' and single_region == 'us-east-1') or (this_profile == 'default' and single_region == 'us-west-2'):
                self.stdout.write("Skipping unused account profile " + this_profile + " and region combo " + single_region)
                continue
            ec2_client = boto3.client('ec2', region_name=single_region)
            self.stdout.write('Acquiring instances in region ' + single_region + ' for profile ' + this_profile + '...')
            inst_list = []
            i=0
            all_instances = ec2_client.describe_instances()['Reservations']
            for reservation in all_instances:
                for instance in reservation['Instances']:
                    i += 1
                    newinstance = False; my_sgs = []; my_tags = []
                    my_id = instance['InstanceId']
                    inst_list.append(my_id)
                    #print "Instance object is " + str(instance)
                    try:
                        x = Instance.objects.get(id=my_id)
                    except Instance.DoesNotExist:
                        newinstance = True
                        x = Instance()
                    x.hypervisor     = instance['Hypervisor'];     x.region_name   = single_region;          x.imageid          = instance['ImageId']; x.id = my_id;
                    x.instancetype   = instance['InstanceType'];   x.launchtime    = instance['LaunchTime']; x.availabilityzone = instance['Placement']['AvailabilityZone']
                    x.privatednsname = instance['PrivateDnsName']; x.publicdnsname = instance['PublicDnsName']
                    x.state = instance['State']['Name']
                    if 'KernelId' in instance: x.kernelid = instance['KernelId']
                    if 'KeyName' in instance:  x.keyname = instance['KeyName']
                    if 'PublicIpAddress' in instance: x.publicipaddress = instance['PublicIpAddress']
                    if 'PrivateIpAddress' in instance: x.privateipaddress = instance['PrivateIpAddress']
                    if 'VpcId' in instance: x.vpcid = instance['VpcId']
                    if 'Tags' in instance:
                        for my_tag in instance['Tags']:
                            my_tags.append(my_tag['Key'] + "  -->  " + my_tag['Value'])
                            if my_tag['Key'] == 'Name': x.name = my_tag['Value']
                    my_tags.sort(); x.tags = "\n".join(my_tags)
                    if 'SecurityGroups' in instance:
                        for my_sg in instance['SecurityGroups']:
                            my_sgs.append(my_sg['GroupName'])
                    my_sgs.sort(); x.securitygroups = "\n".join(my_sgs)
                    with transaction.atomic(), reversion.create_revision():
                        if x.is_dirty():
                            reversion.set_user(User.objects.get(username__exact='admin'))
                            if newinstance:
                                reversion.set_comment('script adding initial revision')
                                self.stdout.write("adding record - " + x.id + " - " + x.name + str(x.get_dirty_fields().keys()))
                            else:
                                reversion.set_comment('script updating fields ' + str(x.get_dirty_fields().keys()))
                                self.stdout.write("updating record - " + x.id + " - " + x.name + str(x.get_dirty_fields().keys()))
                            x.save()
                    if 'Tags' in instance:
                        for my_tag in instance['Tags']:
                            if my_tag['Key'] == 'Product':
                                #print "adding tag for id: " + str(my_id) + " and tag= " + my_tag['Value']
                                ProductTag.objects.update_or_create(name=my_tag['Value'])
                                ProductTagInstance.objects.update_or_create(instance=Instance.objects.get(id=my_id), producttag=ProductTag.objects.get(name=my_tag['Value']))
            # check for potentially deleted instances (pd_instance)
            self.stdout.write('    - found a total of ' + str(len(inst_list)) + ' instance(s)')
            for pd_instance in Instance.objects.filter(region_name=single_region).values_list('id', flat=True):
                if pd_instance not in inst_list:
                    with reversion.create_revision():
                        reversion.set_user(User.objects.get(username__exact='admin'))
                        reversion.set_comment('deleting instance no longer found on AWS')
                        Instance.objects.filter(id=pd_instance).delete()
                        self.stdout.write("Deleting instance not found in list: " + pd_instance)

#  u'OwnerId': '1234567890',
#  u'ReservationId': 'r-1a2b3c4d'},
# {u'Groups': [{u'GroupId': 'sg-1a2b3c4d', u'GroupName': 'Example Group Name'},
#   {u'GroupId': 'sg-2a1b3c4d', u'GroupName': 'Example Group'}],
#  u'Instances': [{u'AmiLaunchIndex': 0,
#    u'Architecture': 'x86_64',
#    u'BlockDeviceMappings': [{u'DeviceName': '/dev/sda1',
#      u'Ebs': {u'AttachTime': datetime.datetime(2015, 5, 7, 19, 29, 29, tzinfo=tzutc()),
#       u'DeleteOnTermination': True,
#       u'Status': 'attached',
#       u'VolumeId': 'vol-d823459f'}}],
#    u'ClientToken': '1a2b3c4d-8951-4311-b651-044123456077_us-east-1c_1',
#    u'EbsOptimized': False,
#    u'Hypervisor': 'xen',
#    u'IamInstanceProfile': {u'Arn': 'arn:aws:iam::1234567890:instance-profile/example',
#     u'Id': 'AIPAJWTWER53ZFTRRTO3U'},
#    u'ImageId': 'ami-9296fb1a',
#    u'InstanceId': 'i-1239f94',
#    u'InstanceType': 'm3.medium',
#    u'KernelId': 'aki-99bb75e1',
#    u'KeyName': 'us-east-example-key',
#    u'LaunchTime': datetime.datetime(2015, 5, 7, 19, 29, 25, tzinfo=tzutc()),
#    u'Monitoring': {u'State': 'enabled'},
#    u'NetworkInterfaces': [],
#    u'Placement': {u'AvailabilityZone': 'us-east-1c',
#     u'GroupName': '',
#     u'Tenancy': 'default'},
#    u'PrivateDnsName': 'ip-10-331-44-190.ec2.internal',
#    u'PrivateIpAddress': '10.331.44.190',
#    u'ProductCodes': [],
#    u'PublicDnsName': 'ec2-54-77-33-253.compute-1.amazonaws.com',
#    u'PublicIpAddress': '54.77.33.253',
#    u'RootDeviceName': '/dev/sda1',
#    u'RootDeviceType': 'ebs',
#    u'SecurityGroups': [{u'GroupId': 'sg-3acdc245',
#      u'GroupName': 'Example Group'},
#     {u'GroupId': 'sg-de1812b5', u'GroupName': 'Example Group'}],
#    u'State': {u'Code': 16, u'Name': 'running'},
#    u'StateTransitionReason': '',
#    u'Tags': [{u'Key': 'aws:autoscaling:groupName',
#      u'Value': 'Example'},
#     {u'Key': 'Component', u'Value': 'tools'},
#     {u'Key': 'Cluster', u'Value': 'ops-tools'},
#     {u'Key': 'Product', u'Value': 'ops'},
#     {u'Key': 'Name', u'Value': 'ops-tools-007'},
#     {u'Key': 'Billing_Code', u'Value': 'ops'},
#     {u'Key': 'Tags', u'Value': 'ps=puppet-blue|pa=puppet'}],
#    u'VirtualizationType': 'paravirtual'}],
#  u'OwnerId': '123459085767',
#  u'RequesterId': '12334221399',
#  u'ReservationId': 'r-75a6f818'}]


