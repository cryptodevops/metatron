from __future__ import unicode_literals
from django.db import models
from django.utils.timezone import now
from django.contrib.auth.models import User
from django.db.models.signals import post_save


class UserProfile(models.Model):
    # This field is required.
    user = models.OneToOneField(User)
    # Other fields here
    first_name = models.CharField(max_length=254)
    last_name = models.CharField(max_length=254)
    my_manager = models.CharField(max_length=254)
    email = models.EmailField(max_length=254)
    telephone = models.CharField(max_length=254)
    title = models.CharField(max_length=254)

def create_user_profile(sender, instance, created, **kwargs):
    UserProfile.objects.get_or_create(user=instance)

post_save.connect(create_user_profile, sender=User)
