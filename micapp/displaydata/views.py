from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from django.http import HttpResponseRedirect, HttpResponse
from django.shortcuts import render
from displaydata.models import UserProfile
import ldap, sys

@login_required
def userinfo(request):
    try:
        ldapuserprofile = UserProfile.objects.get(user_id=request.user.id)
    except UserProfile.DoesNotExist:
        return HttpResponseRedirect('/login/')
    context = {'request': request, 'ldapuser': ldapuserprofile,}
    return render(request, 'displaydata/userinfo.html', context)

@login_required
def userview(request):
   if request.GET.get('u', ''):
      thisuser = request.GET.get('u', '')
   else:
      thisuser = request.user.username
   ldap.set_option(ldap.OPT_REFERRALS, 0)
   ldap.set_option(ldap.OPT_X_TLS_REQUIRE_CERT, 0)
   con = ldap.initialize("ldap://ldapservername_xyx",trace_level=0, trace_file=sys.stdout)

   con.simple_bind_s("ldap_user_xyx","ldap_password_xyx")
   result = con.search_ext_s('dc=toplevel-ldaptree_xyx,dc=local', ldap.SCOPE_SUBTREE, "sAMAccountName=" + thisuser)[0][1]

   mygroups = {}
   counter = 0
   try:
       myname = result['cn']
   except:
       return HttpResponse('username not found in LDAP')
   for group in result['memberOf']:
      counter += 1
      mygroups[counter] = group[group.find("=")+1:group.find(",")]

   context = {'request': request, 'corpgroups': mygroups, 'myresult': result,}
   return render(request, 'groups.html', context)
