#!/bin/bash

mkdir ~/.aws;

curl "https://bootstrap.pypa.io/get-pip.py" -o "get-pip.py";  
python get-pip.py 2>&1 > /dev/null;  
pip install awscli --ignore-installed six 2>&1 > /dev/null;
pip install docker-compose 2>&1 > /dev/null;

echo "[default]\naws_access_key_id=$ECR_ACCESS_KEY_xyx\naws_secret_access_key=$ECR_SECRET_KEY_xyx" > ~/.aws/credentials;  
echo "[default]\nregion=us-east-1" > ~/.aws/config;

login_string=`/usr/local/bin/aws ecr get-login`;

eval $login_string;

docker-compose build;

docker tag local_metatron_latest AMZ_ACCOUNT_ID_xyx.dkr.ecr.us-east-1.amazonaws.com/ecr-name_xyx:metatron_latest
docker push AMZ_ACCOUNT_ID_xyx.dkr.ecr.us-east-1.amazonaws.com/ecr-name_xyx:metatron_latest
aws ecr list-images --repository-name ecr-name_xyx 
docker ps -a
docker info
aws ecs list-tasks --cluster cluster_name_xyx --output text | awk '{print $2}' | xargs aws ecs stop-task --cluster cluster_name_xyx --task
