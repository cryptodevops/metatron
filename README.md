# Metatron - Django, uWSGI and Nginx in a container, using Supervisord

Metatron currently includes a few different apps:

   - **micapp** : the parent Django app
   - **djaws** : the Django AWS inventory app
   - **displaydata** : a utility app used to display data, such as an LDAP user profile, or PagerDuty schedules
   - **jenkinsjobs** : an app to traverse your list of Jenkins servers, indexing jobs, statuses, and an easy way to correlate source repos with Jobs  
   - **testarest** : an implementation of the Django REST framework used for storing test timing results

N.B. There are several places in the code where the string "_xyx" appears, these should be replaced with relevant values
 
The Dockerfile will build a Docker container with a fairly standard and speedy
setup for Django with uWSGI and Nginx.

uWSGI from a number of benchmarks has shown to be the fastest server 
for python applications and allows lots of flexibility. But note that we have
not done any form of optimalization on this package. Modify it to your needs.

Nginx has become the standard for serving up web applications and has the 
additional benefit that it can talk to uWSGI using the uWSGI protocol, further
eliminating overhead.

